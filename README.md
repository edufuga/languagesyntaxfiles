# Natural language syntax files

This small Git repository contains my personal syntax files for the text editor [SublimeText](https://www.sublimetext.com/).

The idea here is to provide syntax highlighting files for coloring words of a *natural language* instead of a considerably smaller set of keywords of a programming or markup language.

The result looks like this:

![](catalan.png)

The syntax files were generated using a personal computer program and my own vocabulary. These files may be used in SublimeText. Concretely, they should go in the folder `/Users/$USER/Library/Application Support/Sublime Text/Packages/User` under macOS, replacing `$USER` by your user name. You can let SublimeText open the right folder for you by pressing `Cmd-Shift-P` and entering `Browser Packages`.

The file ending corresponds to the language the text file is written in. For example, I save catalan documents using the file ending `.cat`. Once this file is opened in SublimeText, the text editor uses the syntax file `cat.sublime-syntax` for highlighting the words according to the different categories (knowledge state). The colors are declared in the file `Mariana.sublime-color-scheme`, which overrides the default dark theme that I use in SublimeText.
